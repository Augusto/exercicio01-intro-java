
public class Animal extends SerVivo{
	private String locomocao;
	private String desenvolvimentoEmbrionario;
	
	public String getLocomocao() {
		return locomocao;
	}
	public void setLocomocao(String locomocao) {
		this.locomocao = locomocao;
	}
	public String getDesenvolvimentoEmbrionario() {
		return desenvolvimentoEmbrionario;
	}
	public void setDesenvolvimentoEmbrionario(String desenvolvimentoEmbrionario) {
		this.desenvolvimentoEmbrionario = desenvolvimentoEmbrionario;
	}
	
	
}
