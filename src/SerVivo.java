
public class SerVivo {
	private String composicaoQuimica;
	private int numeroCelulas;
	private String tipoCelulas;
	
	public String getComposicaoQuimica() {
		return composicaoQuimica;
	}
	public void setComposicaoQuimica(String composicaoQuimica) {
		this.composicaoQuimica = composicaoQuimica;
	}
	public int getNumeroCelulas() {
		return numeroCelulas;
	}
	public void setNumeroCelulas(int numeroCelulas) {
		this.numeroCelulas = numeroCelulas;
	}
	public String getTipoCelulas() {
		return tipoCelulas;
	}
	public void setTipoCelulas(String tipoCelulas) {
		this.tipoCelulas = tipoCelulas;
	}
	
	
}
